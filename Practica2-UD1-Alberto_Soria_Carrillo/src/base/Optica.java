package base;

import java.time.LocalDate;

/**
 * Clase abstracta del ejercicio 2
 *
 * @author Alberto Soria Carrillo
 * @version 1.0
 */
public abstract class Optica {

    // Atributos
    private LocalDate fecha;
    private String nombre;
    private String apellido;
    private String genero;

    // Constructor
    public Optica(String nombre, String apellido, String genero, LocalDate fecha){

        this.nombre = nombre;
        this.apellido = apellido;
        this.genero = genero;
        this.fecha = fecha;

    }
    public Optica(){}

    // Getters and Setters
    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "Optica{" +
                "fecha=" + fecha +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", genero='" + genero + '\'' +
                '}';
    }
}
