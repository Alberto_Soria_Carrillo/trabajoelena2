package base;

import java.time.LocalDate;

/**
 * Clase abtracta Gafas que estiende de Optica
 * @author Alberto Soria Carrillo
 * @see base.Optica
 * @version 1.8
 */
public abstract class Gafas extends Optica{

    // Atributos
    private double izquierdoOjo;
    private double derchoOjo;
    private String uso;
    private String marca;
    private String modelo;
    //Constructor
    public Gafas(String nombre, String apellido, String genero, LocalDate fecha,String uso, double izquierdoOjo, double derchoOjo,String marca, String modelo) {
        super(nombre, apellido, genero, fecha);
        this.izquierdoOjo = izquierdoOjo;
        this.derchoOjo = derchoOjo;
        this.uso = uso;
        this.marca = marca;
        this.modelo = modelo;

    }
    public Gafas(){
        super();
    }
    // Getters and Setters
    public double getIzquierdoOjo() {
        return izquierdoOjo;
    }

    public void setIzquierdoOjo(int izquierdoOjo) {
        this.izquierdoOjo = izquierdoOjo;
    }

    public double getDerchoOjo() {
        return derchoOjo;
    }

    public void setDerchoOjo(int derchoOjo) {
        this.derchoOjo = derchoOjo;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public void setIzquierdoOjo(double izquierdoOjo) {
        this.izquierdoOjo = izquierdoOjo;
    }

    public void setDerchoOjo(double derchoOjo) {
        this.derchoOjo = derchoOjo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Override
    public String toString() {
        return "Gafas{" +
                "izquierdoOjo=" + izquierdoOjo +
                ", derchoOjo=" + derchoOjo +
                ", uso='" + uso + '\'' +
                ", marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                '}';
    }
}
