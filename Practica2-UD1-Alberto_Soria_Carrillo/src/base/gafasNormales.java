package base;

import java.time.LocalDate;

/**
 * Clase gafasNormales extiende de Gafas
 *
 * @author Alberto Soria Carrillo
 * @see base.Gafas
 * @see base.Optica
 * @version 1.8
 */
public class gafasNormales extends Gafas{
    // Atributos
    private String tipo;
    // Constructor
    public gafasNormales(String nombre, String apellido, String genero, LocalDate fecha, String uso, double izquierdoOjo, double derchoOjo, String marca, String modelo) {
        super(nombre, apellido, genero, fecha, uso, izquierdoOjo, derchoOjo, marca, modelo);

        this.tipo = "Normales";
        // Te puesto que esta clase guarde el nombre por defecto para
        // que puedas verificar que hace bien la herencia de manera mas sencilla
    }
    public gafasNormales(){
        super();
    }

    // Getters and Setters
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return  getNombre() + " " +getApellido() + ",gafas de "+ tipo + ", " + getUso();
    }
}
