package main;

import vista.OpticaControlador;
import vista.OpticaModelo;
import vista.Vista;

/**
 * Clase Princial o clase main del programa
 * @author Alberto Soria Carrillo
 * @version 1.8
 */
public class Principal {

    public static void main(String[] args) {
        OpticaControlador controlador = new OpticaControlador(new Vista(),new OpticaModelo());
    }

}
