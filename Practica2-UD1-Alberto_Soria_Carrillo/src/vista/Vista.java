package vista;

import base.Optica;
import com.github.lgooddatepicker.components.DatePicker;
import javax.swing.*;
import java.awt.event.WindowListener;

/**
 * Calse Vista ejecuta el objeto JFrame y los elementos visuales de dicho objeto
 * @author Alberto Soria Carrillo
 * @see java.awt.Frame
 * @version 1.8
 */
public class Vista extends JFrame{
    // Atributos
    private JFrame frame;
    private JPanel panel1;
    private JRadioButton mujerRadioButton;
    private JRadioButton hombreRadioButton;
    private JRadioButton todesRadioButton;
    private JTextField nombreTextField;
    private JTextField apellidoTextField;
    private JTextField marcaTextField;
    private DatePicker fechaDatePicker;
    private JTextField modeloTtextField;
    private JRadioButton normalRadioButton;
    private JRadioButton solRadioButton;
    private JRadioButton ambosRadioButton;
    private JRadioButton cercaRadioButton;
    private JRadioButton legosRadioButton;
    private JRadioButton bifocalesRadioButton;
    private JButton anidarButton;
    private JButton importarButton;
    private JButton exportarButton;
    private JTextArea textArea1;
    private JList list1;
    private JTextField derechoTextField;
    private JTextField izquierdoTextField;

    private DefaultListModel<Optica> optica;

    // Constructor
    public Vista() {
        createUIComponents();
    }

    // Getters and Setters
    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public void setPanel1(JPanel panel1) {
        this.panel1 = panel1;
    }

    public JRadioButton getMujerRadioButton() {
        return mujerRadioButton;
    }

    public void setMujerRadioButton(JRadioButton mujerRadioButton) {
        this.mujerRadioButton = mujerRadioButton;
    }

    public JRadioButton getHombreRadioButton() {
        return hombreRadioButton;
    }

    public void setHombreRadioButton(JRadioButton hombreRadioButton) {
        this.hombreRadioButton = hombreRadioButton;
    }

    public JRadioButton getTodesRadioButton() {
        return todesRadioButton;
    }

    public void setTodesRadioButton(JRadioButton todesRadioButton) {
        this.todesRadioButton = todesRadioButton;
    }

    public JTextField getNombreTextField() {
        return nombreTextField;
    }

    public void setNombreTextField(JTextField nombreTextField) {
        this.nombreTextField = nombreTextField;
    }

    public JTextField getApellidoTextField() {
        return apellidoTextField;
    }

    public void setApellidoTextField(JTextField apellidoTextField) {
        this.apellidoTextField = apellidoTextField;
    }

    public JTextField getMarcaTextField() {
        return marcaTextField;
    }

    public void setMarcaTextField(JTextField marcaTextField) {
        this.marcaTextField = marcaTextField;
    }

    public DatePicker getFechaDatePicker() {
        return fechaDatePicker;
    }

    public void setFechaDatePicker(DatePicker fechaDatePicker) {
        this.fechaDatePicker = fechaDatePicker;
    }

    public JTextField getModeloTextField() {
        return modeloTtextField;
    }

    public void setModeloTtextField(JTextField modeloTtextField) {
        this.modeloTtextField = modeloTtextField;
    }

    public JRadioButton getNormalRadioButton() {
        return normalRadioButton;
    }

    public void setNormalRadioButton(JRadioButton normalRadioButton) {
        this.normalRadioButton = normalRadioButton;
    }

    public JRadioButton getSolRadioButton() {
        return solRadioButton;
    }

    public void setSolRadioButton(JRadioButton solRadioButton) {
        this.solRadioButton = solRadioButton;
    }

    public JRadioButton getAmbosRadioButton() {
        return ambosRadioButton;
    }

    public void setAmbosRadioButton(JRadioButton ambosRadioButton) {
        this.ambosRadioButton = ambosRadioButton;
    }

    public JRadioButton getCercaRadioButton() {
        return cercaRadioButton;
    }

    public void setCercaRadioButton(JRadioButton cercaRadioButton) {
        this.cercaRadioButton = cercaRadioButton;
    }

    public JRadioButton getLejosRadioButton() {
        return legosRadioButton;
    }

    public void setLegosRadioButton(JRadioButton legosRadioButton) {
        this.legosRadioButton = legosRadioButton;
    }

    public JRadioButton getBifocalesRadioButton() {
        return bifocalesRadioButton;
    }

    public void setBifocalesRadioButton(JRadioButton bifocalesRadioButton) {
        this.bifocalesRadioButton = bifocalesRadioButton;
    }

    public JButton getAnidarButton() {
        return anidarButton;
    }

    public void setAnidarButton(JButton anidarButton) {
        this.anidarButton = anidarButton;
    }

    public JButton getImportarButton() {
        return importarButton;
    }

    public void setImportarButton(JButton importarButton) {
        this.importarButton = importarButton;
    }

    public JButton getExportarButton() {
        return exportarButton;
    }

    public void setExportarButton(JButton exportarButton) {
        this.exportarButton = exportarButton;
    }

    public JTextArea getTextArea1() {
        return textArea1;
    }

    public void setTextArea1(JTextArea textArea1) {
        this.textArea1 = textArea1;
    }

    public JList getList1() {
        return list1;
    }

    public JTextField getDerechoTextField() {
        return derechoTextField;
    }

    public void setDerechoTextField(JTextField derechoTextField) {
        this.derechoTextField = derechoTextField;
    }

    public JTextField getIzquierdoTextField() {
        return izquierdoTextField;
    }

    public void setIzquierdoTextField(JTextField izquierdoTextField) {
        this.izquierdoTextField = izquierdoTextField;
    }

    public void setList1(JList list1) {
        this.list1 = list1;
    }

    public DefaultListModel<Optica> getOptica() {
        return optica;
    }

    public void setOptica(DefaultListModel<Optica> optica) {
        this.optica = optica;
    }

    /**
     * Añade los elementos y atributos necesarios de la ventana
     */
    private void createUIComponents() {

        JFrame frame = new JFrame("Optica");
        frame.add(panel1);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(600,400);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        inicializarElementos();

    }

    /**
     * Inicia los Elementos necesarios del programa
     */
    private void inicializarElementos() {

        optica = new DefaultListModel<Optica>();
        list1.setModel(optica);

    }

}
