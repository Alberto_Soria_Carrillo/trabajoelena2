package vista;

import base.*;
import org.xml.sax.SAXException;
import util.Util;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileReader;
import java.io.*;
import java.util.Properties;

/**
 * Clase OpticaControlador se ocupa del control de las acciones de la aplicacion
 * @author Alberto Soria Carrillo
 * @see java.awt.event.ActionListener
 * @see java.awt.event.WindowListener
 * @see java.util.EventListener
 * @see javax.swing.event.ListSelectionListener
 */
public class OpticaControlador implements ActionListener, ListSelectionListener, WindowListener {

    // Atributos
    private final Vista vista;
    private final OpticaModelo opticaModelo;
    private File ultimaRutaExportada;

    //Constructos
    public OpticaControlador(Vista vista, OpticaModelo opticaModelo) {
        this.vista = vista;
        this.opticaModelo = opticaModelo;

        try {
            this.cargarDatosConfiguracion();
        } catch (IOException e) {
            System.out.println("Fallo en la carga de datos de configuracion");
        }

        this.addActionListener(this);
        this.addListSelectionListener(this);
        this.addWindowListener(this);

    }

    /**
     * Escucha los eventos de la ventana
     * @param listener
     */
    private void addWindowListener(WindowListener listener) {
        vista.getFrame().addWindowListener(listener);
    }

    /**
     * Escucha los eventos de ls Lista
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener) {
        vista.getList1().addListSelectionListener(listener);
    }

    /**
     * Agrega elementos de escucha de eventos a los botones
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        vista.getMujerRadioButton().addActionListener(listener);
        vista.getHombreRadioButton().addActionListener(listener);
        vista.getTodesRadioButton().addActionListener(listener); // No pude resistirme a ponerlo
        vista.getNormalRadioButton().addActionListener(listener);
        vista.getSolRadioButton().addActionListener(listener);
        vista.getAmbosRadioButton().addActionListener(listener);
        vista.getCercaRadioButton().addActionListener(listener);
        vista.getLejosRadioButton().addActionListener(listener);
        vista.getBifocalesRadioButton().addActionListener(listener);
        vista.getAnidarButton().addActionListener(listener);
        vista.getImportarButton().addActionListener(listener);
        vista.getExportarButton().addActionListener(listener);

    }

    /**
     * Carga los datos de la configuración
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("Optica.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }

    /**
     * Guarda los datos de la configuración
     * @throws IOException
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("Optica.conf"), "Datos optica");

    }

    /**
     * Limpia los campos
     */
    private void limpiarCampos() {

        vista.getNombreTextField().setText(null);
        vista.getApellidoTextField().setText(null);
        vista.getMarcaTextField().setText(null);
        vista.getModeloTextField().setText(null);
        vista.getIzquierdoTextField().setText(null);
        vista.getDerechoTextField().setText(null);
        vista.getFechaDatePicker().setText(null);

    }

    /**
     * Refresca la lista de clientes
     */
    private void refrescar() {
        vista.getOptica().clear();
        for (Optica aux : opticaModelo.obtenerClienteGafas()) {
            vista.getOptica().addElement(aux);
        }
    }

    /**
     * Actualiza los datos de configuracion
     * @param ultimaRutaExportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada) {
        this.ultimaRutaExportada = ultimaRutaExportada;
    }

    /**
     * Verifica si existen campos vacios
     * @return boolean
     */
    private boolean hayCamposVacios() {
        if( vista.getNombreTextField() == null ||
                vista.getApellidoTextField() == null ||
                vista.getMarcaTextField() == null ||
                vista.getModeloTextField() == null ||
                vista.getIzquierdoTextField() == null ||
                vista.getDerechoTextField() == null ||
                vista.getFechaDatePicker() == null){
            return true;
        }
        return false;
    }

    // Metodos heredados
    @Override
    public void actionPerformed(ActionEvent e) {

        String acctionComand = e.getActionCommand();

        switch (acctionComand){
            case "Añadir":

                if (hayCamposVacios()) {
                    Util.mensajeError("Los siguientes campos no pueden estar vacios\n" +
                            "Nombre\nApellido\nMarca\nModelo\nFecha\nOjo izquierdo\nOjo Derecho" +
                            vista.getTextArea1().getText());
                    break;
                }

                if (opticaModelo.existeCliente(vista.getNombreTextField().getText())) {
                    Util.mensajeError("Ya existe este cliente" +
                            vista.getTextArea1().getText());
                    break;
                }

                String uso ="";

                if (vista.getCercaRadioButton().isSelected()){
                    uso = "Cerca";
                }
                if (vista.getLejosRadioButton().isSelected()){
                    uso = "Lejos";
                }
                if (vista.getBifocalesRadioButton().isSelected()){
                    uso = "Bifocales";
                }

                String genero = "";

                if(vista.getMujerRadioButton().isSelected()){
                    genero = "Mujer";
                }
                if (vista.getHombreRadioButton().isSelected()){
                    genero = "Hombre";
                }
                if (vista.getTodesRadioButton().isSelected()){
                    genero = "Todes";
                }

                double izqr;
                double derch;

                try {
                    izqr = Double.parseDouble(vista.getIzquierdoTextField().getText());
                    derch = Double.parseDouble(vista.getDerechoTextField().getText());
                }catch (Exception e1) {

                    Util.mensajeError("Los campos derecho e izquierdo solo aceptan datos numericos" +
                            vista.getTextArea1().getText());
                    break;
                }

                if (vista.getSolRadioButton().isSelected()) {
                    opticaModelo.altaGafasSol(vista.getNombreTextField().getText(), vista.getApellidoTextField().getText(), genero,
                            vista.getFechaDatePicker().getDate(), uso, izqr,derch, vista.getMarcaTextField().getText(),
                            vista.getModeloTextField().getText());
                }

                if (vista.getNormalRadioButton().isSelected()) {
                    opticaModelo.altaGafasNormales(vista.getNombreTextField().getText(), vista.getApellidoTextField().getText(), genero,
                            vista.getFechaDatePicker().getDate(), uso, izqr,derch, vista.getMarcaTextField().getText(),
                            vista.getModeloTextField().getText());
                }

                if (vista.getAmbosRadioButton().isSelected()) {
                    opticaModelo.altaAmbos(vista.getNombreTextField().getText(), vista.getApellidoTextField().getText(), genero,
                            vista.getFechaDatePicker().getDate(), uso, izqr,derch, vista.getMarcaTextField().getText(),
                            vista.getModeloTextField().getText());
                }


                refrescar();
                limpiarCampos();

                break;
            case "Importar":
                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML", "xml");
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        opticaModelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                }
                refrescar();
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML", "xml");
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        opticaModelo.exportarXML(selectorFichero2.getSelectedFile());
                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
        }

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Optica optica = (Optica) vista.getList1().getSelectedValue();

            vista.getNombreTextField().setText((optica.getNombre()));
            vista.getApellidoTextField().setText(optica.getApellido());
            vista.getFechaDatePicker().setDate(optica.getFecha());

            if (optica instanceof gafasSol) {
                vista.getSolRadioButton().doClick();
                if (((gafasSol) vista.getList1().getSelectedValue()).getGenero().equalsIgnoreCase("Mujer")){
                    vista.getMujerRadioButton().doClick();
                } else if (((gafasSol) vista.getList1().getSelectedValue()).getGenero().equalsIgnoreCase("Hombre")){
                    vista.getHombreRadioButton().doClick();
                } else {
                    vista.getTodesRadioButton().doClick();
                }
                if(((gafasSol) vista.getList1().getSelectedValue()).getUso().equalsIgnoreCase("Cerca")){
                    vista.getCercaRadioButton().doClick();
                }else if (((gafasSol) vista.getList1().getSelectedValue()).getUso().equalsIgnoreCase("Lejos")){
                    vista.getLejosRadioButton().doClick();
                }else {
                    vista.getAmbosRadioButton().doClick();
                }
                vista.getMarcaTextField().setText(((gafasSol) optica).getMarca());
                vista.getModeloTextField().setText(((gafasSol) optica).getModelo());
                vista.getIzquierdoTextField().setText(String.valueOf(((gafasSol) optica).getIzquierdoOjo()));
                vista.getDerechoTextField().setText(String.valueOf(((gafasSol) optica).getDerchoOjo()));
            } else if (optica instanceof gafasNormales){
                vista.getNormalRadioButton().doClick();
                if (((gafasNormales) vista.getList1().getSelectedValue()).getGenero().equalsIgnoreCase("Mujer")){
                    vista.getMujerRadioButton().doClick();
                } else if (((gafasNormales) vista.getList1().getSelectedValue()).getGenero().equalsIgnoreCase("Hombre")){
                    vista.getHombreRadioButton().doClick();
                } else {
                    vista.getTodesRadioButton().doClick();
                }
                if(((gafasNormales) vista.getList1().getSelectedValue()).getUso().equalsIgnoreCase("Cerca")){
                    vista.getCercaRadioButton().doClick();
                }else if (((gafasNormales) vista.getList1().getSelectedValue()).getUso().equalsIgnoreCase("Lejos")){
                    vista.getLejosRadioButton().doClick();
                }else {
                    vista.getAmbosRadioButton().doClick();
                }
                vista.getMarcaTextField().setText(((gafasNormales) optica).getMarca());
                vista.getModeloTextField().setText(((gafasNormales) optica).getModelo());
                vista.getIzquierdoTextField().setText(String.valueOf(((gafasNormales) optica).getIzquierdoOjo()));
                vista.getDerechoTextField().setText(String.valueOf(((gafasNormales) optica).getDerchoOjo()));
            } else {
                vista.getAmbosRadioButton().doClick();
                if (((gafasAmbos) vista.getList1().getSelectedValue()).getGenero().equalsIgnoreCase("Mujer")){
                    vista.getMujerRadioButton().doClick();
                } else if (((gafasAmbos) vista.getList1().getSelectedValue()).getGenero().equalsIgnoreCase("Hombre")){
                    vista.getHombreRadioButton().doClick();
                } else {
                    vista.getTodesRadioButton().doClick();
                }
                if(((gafasAmbos) vista.getList1().getSelectedValue()).getUso().equalsIgnoreCase("Cerca")){
                    vista.getCercaRadioButton().doClick();
                }else if (((gafasAmbos) vista.getList1().getSelectedValue()).getUso().equalsIgnoreCase("Lejos")){
                    vista.getLejosRadioButton().doClick();
                }else {
                    vista.getAmbosRadioButton().doClick();
                }
                vista.getMarcaTextField().setText(((gafasAmbos) optica).getMarca());
                vista.getModeloTextField().setText(((gafasAmbos) optica).getModelo());
                vista.getIzquierdoTextField().setText(String.valueOf(((gafasAmbos) optica).getIzquierdoOjo()));
                vista.getDerechoTextField().setText(String.valueOf(((gafasAmbos) optica).getDerchoOjo()));

            }

        }
    }

    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }


    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

}
