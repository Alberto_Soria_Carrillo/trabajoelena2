package vista;

import base.Optica;
import base.gafasAmbos;
import base.gafasNormales;
import base.gafasSol;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase OpticaModel se ocupa en la uniformidad del tratamiento de la informacion
 * @author Alberto Soria Carrillo
 *
 */
public class OpticaModelo {

    // Atributos
    private ArrayList<Optica> opticas;
    // Constructor
    public OpticaModelo() {
        opticas = new ArrayList<Optica>();
    }

    /**
     * Debuelve el ArrayList
     * @return
     */
    public ArrayList<Optica> obtenerClienteGafas() {
        return opticas;
    }

    /**
     * Añade un nuevo elemento al ArayList
     * @param nombre
     * @param apellido
     * @param genero
     * @param fecha
     * @param uso
     * @param izquierdoOjo
     * @param derchoOjo
     * @param marca
     * @param modelo
     */
    public void altaGafasSol(String nombre, String apellido, String genero, LocalDate fecha, String uso, double izquierdoOjo, double derchoOjo, String marca, String modelo) {
        gafasSol nuevaGafa = new gafasSol(nombre, apellido, genero, fecha, uso, izquierdoOjo, derchoOjo, marca, modelo);
        opticas.add(nuevaGafa);
    }
    /**
     * Añade un nuevo elemento al ArayList
     * @param nombre
     * @param apellido
     * @param genero
     * @param fecha
     * @param uso
     * @param izquierdoOjo
     * @param derchoOjo
     * @param marca
     * @param modelo
     */
    public void altaGafasNormales(String nombre, String apellido, String genero, LocalDate fecha, String uso, double izquierdoOjo, double derchoOjo, String marca, String modelo) {
        gafasNormales nuevaGafa = new gafasNormales(nombre, apellido, genero, fecha, uso, izquierdoOjo, derchoOjo, marca, modelo);
        opticas.add(nuevaGafa);
    }
    /**
     * Añade un nuevo elemento al ArayList
     * @param nombre
     * @param apellido
     * @param genero
     * @param fecha
     * @param uso
     * @param izquierdoOjo
     * @param derchoOjo
     * @param marca
     * @param modelo
     */
    public void altaAmbos(String nombre, String apellido, String genero, LocalDate fecha, String uso, double izquierdoOjo, double derchoOjo, String marca, String modelo) {
        gafasAmbos nuevaGafa = new gafasAmbos(nombre, apellido, genero, fecha, uso, izquierdoOjo, derchoOjo, marca, modelo);
        opticas.add(nuevaGafa);
    }

    /**
     * Comprueba si existe el cliente
     * @param nombre
     * @return boolean
     */
    public boolean existeCliente(String nombre) {
        for (Optica aux : opticas) {
            if (aux.getNombre().equalsIgnoreCase(nombre)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Exporta un fichero XML
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Opticas");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoOptica = null;
        Element nodoDatos = null;
        Text texto = null;

        for (Optica auxOptica : opticas) {
            if (auxOptica instanceof gafasNormales) {
                nodoOptica = documento.createElement("Normales");

            } else if (auxOptica instanceof  gafasSol){
                nodoOptica = documento.createElement("Sol");
            }else{
                nodoOptica = documento.createElement("Ambos");
            }
            raiz.appendChild(nodoOptica);

            nodoDatos = documento.createElement("Nombre");
            nodoOptica.appendChild(nodoDatos);
            texto = documento.createTextNode(auxOptica.getNombre());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Apellido");
            nodoOptica.appendChild(nodoDatos);
            texto = documento.createTextNode(auxOptica.getApellido());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Genero");
            nodoOptica.appendChild(nodoDatos);
            texto = documento.createTextNode(auxOptica.getGenero());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("Fecha");
            nodoOptica.appendChild(nodoDatos);
            texto = documento.createTextNode(auxOptica.getFecha().toString());
            nodoDatos.appendChild(texto);

            if (auxOptica instanceof gafasSol) {
                nodoDatos = documento.createElement("Tipo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasSol) auxOptica).getTipo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Marca");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasSol) auxOptica).getMarca());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Modelo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasSol) auxOptica).getModelo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("OjoIzquierdo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((gafasSol) auxOptica).getIzquierdoOjo()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("OjoDerecho");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((gafasSol) auxOptica).getDerchoOjo()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Uso");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasSol) auxOptica).getUso());
                nodoDatos.appendChild(texto);

            } else if (auxOptica instanceof gafasNormales) {
                nodoDatos = documento.createElement("Tipo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasNormales) auxOptica).getTipo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Marca");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasNormales) auxOptica).getMarca());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Modelo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasNormales) auxOptica).getModelo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("OjoIzquierdo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((gafasNormales) auxOptica).getIzquierdoOjo()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("OjoDerecho");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((gafasNormales) auxOptica).getDerchoOjo()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Uso");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasNormales) auxOptica).getUso());
                nodoDatos.appendChild(texto);

            }else {
                nodoDatos = documento.createElement("Tipo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasAmbos) auxOptica).getTipo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Marca");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasAmbos) auxOptica).getMarca());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Modelo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasAmbos) auxOptica).getModelo());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("OjoIzquierdo");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((gafasAmbos) auxOptica).getIzquierdoOjo()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("OjoDerecho");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((gafasAmbos) auxOptica).getDerchoOjo()));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("Uso");
                nodoOptica.appendChild(nodoDatos);
                texto = documento.createTextNode(((gafasAmbos) auxOptica).getUso());
                nodoDatos.appendChild(texto);

            }

        }
        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }


    /**
     * Importa un fichero XML
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {

        opticas = new ArrayList<Optica>();
        gafasNormales auxNormales = null;
        gafasSol auxSol = null;
        gafasAmbos auxAmbos = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoElemento = (Element) listaElementos.item(i);

            if (nodoElemento.getTagName().equalsIgnoreCase("Normales")) {
                auxNormales = new gafasNormales();
                auxNormales.setNombre(nodoElemento.getChildNodes().item(0).getTextContent());
                auxNormales.setApellido(nodoElemento.getChildNodes().item(1).getTextContent());
                auxNormales.setGenero(nodoElemento.getChildNodes().item(2).getTextContent());
                auxNormales.setFecha(LocalDate.parse(nodoElemento.getChildNodes().item(3).getTextContent()));
                auxNormales.setTipo(nodoElemento.getChildNodes().item(4).getTextContent());
                auxNormales.setMarca(nodoElemento.getChildNodes().item(5).getTextContent());
                auxNormales.setModelo(nodoElemento.getChildNodes().item(6).getTextContent());
                auxNormales.setIzquierdoOjo(Double.parseDouble(nodoElemento.getChildNodes().item(7).getTextContent()));
                auxNormales.setDerchoOjo(Double.parseDouble(nodoElemento.getChildNodes().item(8).getTextContent()));
                auxNormales.setUso(nodoElemento.getChildNodes().item(9).getTextContent());

                opticas.add(auxNormales);
            } else if (nodoElemento.getTagName().equalsIgnoreCase("Sol")){
                    auxSol = new gafasSol();

                    auxSol.setNombre(nodoElemento.getChildNodes().item(0).getTextContent());
                    auxSol.setApellido(nodoElemento.getChildNodes().item(1).getTextContent());
                    auxSol.setGenero(nodoElemento.getChildNodes().item(2).getTextContent());
                    auxSol.setFecha(LocalDate.parse(nodoElemento.getChildNodes().item(3).getTextContent()));
                    auxSol.setTipo(nodoElemento.getChildNodes().item(4).getTextContent());
                    auxSol.setMarca(nodoElemento.getChildNodes().item(5).getTextContent());
                    auxSol.setModelo(nodoElemento.getChildNodes().item(6).getTextContent());
                    auxSol.setIzquierdoOjo(Double.parseDouble(nodoElemento.getChildNodes().item(7).getTextContent()));
                    auxSol.setDerchoOjo(Double.parseDouble(nodoElemento.getChildNodes().item(8).getTextContent()));
                    auxSol.setUso(nodoElemento.getChildNodes().item(9).getTextContent());

                    opticas.add(auxSol);
            }else{
                if (nodoElemento.getTagName().equalsIgnoreCase("Ambos")) {
                    auxAmbos = new gafasAmbos();

                    auxAmbos.setNombre(nodoElemento.getChildNodes().item(0).getTextContent());
                    auxAmbos.setApellido(nodoElemento.getChildNodes().item(1).getTextContent());
                    auxAmbos.setGenero(nodoElemento.getChildNodes().item(2).getTextContent());
                    auxAmbos.setFecha(LocalDate.parse(nodoElemento.getChildNodes().item(3).getTextContent()));
                    auxAmbos.setTipo(nodoElemento.getChildNodes().item(4).getTextContent());
                    auxAmbos.setMarca(nodoElemento.getChildNodes().item(5).getTextContent());
                    auxAmbos.setModelo(nodoElemento.getChildNodes().item(6).getTextContent());
                    auxAmbos.setIzquierdoOjo(Double.parseDouble(nodoElemento.getChildNodes().item(7).getTextContent()));
                    auxAmbos.setDerchoOjo(Double.parseDouble(nodoElemento.getChildNodes().item(8).getTextContent()));
                    auxAmbos.setUso(nodoElemento.getChildNodes().item(9).getTextContent());

                    opticas.add(auxAmbos);
                }
            }
        }
    }
}
